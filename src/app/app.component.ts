import { Component } from '@angular/core';
import {MdTabsModule} from '@angular/material';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  friends = [ 'Carlos' , 'Marina' , 'Mateus'];
  newFriend = '';
  friendStatus: String = 'offline';

  constructor() {}

  random = function(){
    this.friendStatus = Math.random() >  0.5 ? 'online' : 'offline';
    return this.friendStatus;
  }

  pushFriend = function(index){
    if (this.newFriend !== '') {
      this.friends.push(this.newFriend);
      this.newFriend = '';
      }
  }
  removeFriend = function(index){
    this.friends.splice(index, 1);
  }
}
